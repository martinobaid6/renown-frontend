import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import {MatIconModule} from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { SecondSectionComponent } from './second-section/second-section.component';
import {VgCoreModule} from '@videogular/ngx-videogular/core';
import {VgControlsModule} from '@videogular/ngx-videogular/controls';
import {YouTubePlayerModule} from '@angular/youtube-player';
import { ThirdSectionComponent } from './third-section/third-section.component';
import { MailingListComponent } from './mailing-list/mailing-list.component';
import { FooterComponent } from './footer/footer.component';
import {CrystalLightboxModule} from '../ivybox-ng8+/lightbox.module';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {IvyCarouselModule} from '../ivyсarousel_pro/carousel.module';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {DialogOverviewExampleDialog} from './shared/shared.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SecondSectionComponent,
    ThirdSectionComponent,
    MailingListComponent,
    FooterComponent,
    DialogOverviewExampleDialog
  ],
  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    MatIconModule,
    BrowserAnimationsModule,
    HttpClientModule,
    VgCoreModule,
    VgControlsModule,
    YouTubePlayerModule,
    CrystalLightboxModule,
    LazyLoadImageModule,
    IvyCarouselModule,
    MatDialogModule,
    MatExpansionModule
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
