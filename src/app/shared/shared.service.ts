import {Component, Injectable} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  constructor(public dialog: MatDialog) { }

  openDialog(): MatDialogRef<DialogOverviewExampleDialog, any> {
    return this.dialog.open(DialogOverviewExampleDialog, {
      backdropClass: 'cdk-overlay-transparent-backdrop',
      hasBackdrop: true,
      panelClass: 'w-1/2'
    });
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
