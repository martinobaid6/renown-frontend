import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {SharedService} from '../shared/shared.service';

@Component({
  selector: 'app-second-section',
  templateUrl: './second-section.component.html',
  styleUrls: ['./second-section.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecondSectionComponent {
  constructor(public shared: SharedService) {}

  public playVideo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  playVid(): void {
    this.playVideo.value ? this.playVideo.next(false) : this.playVideo.next(true);
  }

}
