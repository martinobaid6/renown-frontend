import {ChangeDetectorRef, Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-third-section',
  templateUrl: './third-section.component.html',
  styleUrls: ['./third-section.component.scss']
})
export class ThirdSectionComponent implements OnInit {
  memberCount: number;
  presenceCount: number;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.http.get<{ approximate_member_count: number, approximate_presence_count: number  }>('https://discord.com/api/v8/invites/Tr2DxFh?with_counts=true')
        .subscribe(resp => {
          this.memberCount = resp.approximate_member_count;
          this.presenceCount = resp.approximate_presence_count;
          this.cdr.markForCheck();
        });
    }
  }

}
