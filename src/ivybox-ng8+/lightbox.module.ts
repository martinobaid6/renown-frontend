import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LightboxCommonComponent } from './lightbox-common.component';
import { LightboxComponent } from './lightbox.component';
import { LightboxTouchscreenComponent } from './lightbox-touchscreen.component';
import { CrystalLightbox } from'./lightbox.service' 
import { EventService } from './event.service';
import { PinchZoomComponent } from './pinch-zoom.component'; 
import { LightboxDirective } from'./lightbox.directive';
import { LightboxGroupDirective } from'./lightbox-group.directive';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        LightboxCommonComponent,
        LightboxComponent,
        LightboxTouchscreenComponent,
        PinchZoomComponent,
        LightboxDirective,
        LightboxGroupDirective
    ],
    imports: [
        CommonModule,
        //BrowserAnimationsModule
    ],
    exports: [
        LightboxDirective,
        LightboxGroupDirective,
        PinchZoomComponent
    ],
    providers: [
        CrystalLightbox,
        EventService
    ],
    bootstrap: [
        
    ],
    entryComponents: [
        LightboxComponent,
        LightboxTouchscreenComponent,
        PinchZoomComponent
    ]
})
export class CrystalLightboxModule { }
